<?php

namespace app\controllers;

use app\models\Suenan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SuenanController implements the CRUD actions for Suenan model.
 */
class SuenanController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Suenan models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Suenan::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_suenan' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Suenan model.
     * @param int $codigo_suenan Codigo Suenan
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_suenan)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_suenan),
        ]);
    }

    /**
     * Creates a new Suenan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Suenan();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo_suenan' => $model->codigo_suenan]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Suenan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo_suenan Codigo Suenan
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_suenan)
    {
        $model = $this->findModel($codigo_suenan);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_suenan' => $model->codigo_suenan]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Suenan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo_suenan Codigo Suenan
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_suenan)
    {
        $this->findModel($codigo_suenan)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Suenan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo_suenan Codigo Suenan
     * @return Suenan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_suenan)
    {
        if (($model = Suenan::findOne(['codigo_suenan' => $codigo_suenan])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
