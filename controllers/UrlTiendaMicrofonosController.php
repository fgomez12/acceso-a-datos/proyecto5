<?php

namespace app\controllers;

use app\models\UrlTiendaMicrofonos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UrlTiendaMicrofonosController implements the CRUD actions for UrlTiendaMicrofonos model.
 */
class UrlTiendaMicrofonosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all UrlTiendaMicrofonos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => UrlTiendaMicrofonos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_tienda_microfonos' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UrlTiendaMicrofonos model.
     * @param int $codigo_tienda_microfonos Codigo Tienda Microfonos
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_tienda_microfonos)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_tienda_microfonos),
        ]);
    }

    /**
     * Creates a new UrlTiendaMicrofonos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new UrlTiendaMicrofonos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo_tienda_microfonos' => $model->codigo_tienda_microfonos]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing UrlTiendaMicrofonos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo_tienda_microfonos Codigo Tienda Microfonos
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_tienda_microfonos)
    {
        $model = $this->findModel($codigo_tienda_microfonos);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_tienda_microfonos' => $model->codigo_tienda_microfonos]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UrlTiendaMicrofonos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo_tienda_microfonos Codigo Tienda Microfonos
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_tienda_microfonos)
    {
        $this->findModel($codigo_tienda_microfonos)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UrlTiendaMicrofonos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo_tienda_microfonos Codigo Tienda Microfonos
     * @return UrlTiendaMicrofonos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_tienda_microfonos)
    {
        if (($model = UrlTiendaMicrofonos::findOne(['codigo_tienda_microfonos' => $codigo_tienda_microfonos])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
