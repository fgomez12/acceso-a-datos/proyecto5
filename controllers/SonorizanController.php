<?php

namespace app\controllers;

use app\models\Sonorizan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SonorizanController implements the CRUD actions for Sonorizan model.
 */
class SonorizanController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Sonorizan models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Sonorizan::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_sonorizan' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sonorizan model.
     * @param int $codigo_sonorizan Codigo Sonorizan
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_sonorizan)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_sonorizan),
        ]);
    }

    /**
     * Creates a new Sonorizan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Sonorizan();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo_sonorizan' => $model->codigo_sonorizan]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Sonorizan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo_sonorizan Codigo Sonorizan
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_sonorizan)
    {
        $model = $this->findModel($codigo_sonorizan);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_sonorizan' => $model->codigo_sonorizan]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sonorizan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo_sonorizan Codigo Sonorizan
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_sonorizan)
    {
        $this->findModel($codigo_sonorizan)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sonorizan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo_sonorizan Codigo Sonorizan
     * @return Sonorizan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_sonorizan)
    {
        if (($model = Sonorizan::findOne(['codigo_sonorizan' => $codigo_sonorizan])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
