<?php

namespace app\controllers;

use app\models\UrlTiendaInstrumentos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UrlTiendaInstrumentosController implements the CRUD actions for UrlTiendaInstrumentos model.
 */
class UrlTiendaInstrumentosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all UrlTiendaInstrumentos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => UrlTiendaInstrumentos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_tienda_instrumento' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UrlTiendaInstrumentos model.
     * @param int $codigo_tienda_instrumento Codigo Tienda Instrumento
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_tienda_instrumento)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_tienda_instrumento),
        ]);
    }

    /**
     * Creates a new UrlTiendaInstrumentos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new UrlTiendaInstrumentos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo_tienda_instrumento' => $model->codigo_tienda_instrumento]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing UrlTiendaInstrumentos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo_tienda_instrumento Codigo Tienda Instrumento
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_tienda_instrumento)
    {
        $model = $this->findModel($codigo_tienda_instrumento);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_tienda_instrumento' => $model->codigo_tienda_instrumento]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UrlTiendaInstrumentos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo_tienda_instrumento Codigo Tienda Instrumento
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_tienda_instrumento)
    {
        $this->findModel($codigo_tienda_instrumento)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UrlTiendaInstrumentos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo_tienda_instrumento Codigo Tienda Instrumento
     * @return UrlTiendaInstrumentos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_tienda_instrumento)
    {
        if (($model = UrlTiendaInstrumentos::findOne(['codigo_tienda_instrumento' => $codigo_tienda_instrumento])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
