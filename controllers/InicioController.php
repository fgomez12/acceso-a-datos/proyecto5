<?php

namespace app\controllers;
use app\models\Canciones;
use yii\web\Controller;

class InicioController extends \yii\web\Controller
{
    public function actionIniciocontroller()
    {
        $canciones = Canciones::find()->all();
    return $this->render('index', [ 
        'canciones' => $canciones
    ]); 
    }

}