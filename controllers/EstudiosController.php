<?php

namespace app\controllers;

use app\models\Estudios;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EstudiosController implements the CRUD actions for Estudios model.
 */
class EstudiosController extends Controller
{
    
    public function getEstudioName( $codigo_estudio ) {
        //$estudio = $this->findModel($codigo_estudio);
        return "HOLAQUETAL: " . $codigo_estudio;
    }
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Estudios models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Estudios::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_estudio' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Estudios model.
     * @param int $codigo_estudio Codigo Estudio
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_estudio)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_estudio),
        ]);
    }

    /**
     * Creates a new Estudios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Estudios();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo_estudio' => $model->codigo_estudio]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Estudios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo_estudio Codigo Estudio
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_estudio)
    {
        $model = $this->findModel($codigo_estudio);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_estudio' => $model->codigo_estudio]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Estudios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo_estudio Codigo Estudio
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_estudio)
    {
        $this->findModel($codigo_estudio)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Estudios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo_estudio Codigo Estudio
     * @return Estudios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_estudio)
    {
        if (($model = Estudios::findOne(['codigo_estudio' => $codigo_estudio])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    
}
