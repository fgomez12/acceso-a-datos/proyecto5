<?php

namespace app\controllers;

use app\models\Microfonos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MicrofonosController implements the CRUD actions for Microfonos model.
 */
class MicrofonosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Microfonos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Microfonos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_microfono' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Microfonos model.
     * @param int $codigo_microfono Codigo Microfono
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo_microfono)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigo_microfono),
        ]);
    }

    /**
     * Creates a new Microfonos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Microfonos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo_microfono' => $model->codigo_microfono]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Microfonos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo_microfono Codigo Microfono
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo_microfono)
    {
        $model = $this->findModel($codigo_microfono);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo_microfono' => $model->codigo_microfono]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Microfonos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo_microfono Codigo Microfono
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo_microfono)
    {
        $this->findModel($codigo_microfono)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Microfonos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo_microfono Codigo Microfono
     * @return Microfonos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo_microfono)
    {
        if (($model = Microfonos::findOne(['codigo_microfono' => $codigo_microfono])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
