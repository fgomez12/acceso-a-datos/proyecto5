<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Microfonos $model */

$this->title = 'Create Microfonos';
$this->params['breadcrumbs'][] = ['label' => 'Microfonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="microfonos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
