<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Microfonos $model */

$this->title = 'Update Microfonos: ' . $model->codigo_microfono;
$this->params['breadcrumbs'][] = ['label' => 'Microfonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_microfono, 'url' => ['view', 'codigo_microfono' => $model->codigo_microfono]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="microfonos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
