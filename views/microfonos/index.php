<?php

use app\models\Microfonos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Micrófonos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="microfonos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Microfóno', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'codigo_microfono',
            'tipo',
            'marca',
            'modelo',
            //'codigo_tienda_microfonos',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Microfonos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo_microfono' => $model->codigo_microfono]);
                 }
            ],
        ],
    ]); ?>


</div>
