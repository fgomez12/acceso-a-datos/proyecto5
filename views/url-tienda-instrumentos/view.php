<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\UrlTiendaInstrumentos $model */

$this->title = $model->codigo_tienda_instrumento;
$this->params['breadcrumbs'][] = ['label' => 'Url Tienda Instrumentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="url-tienda-instrumentos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigo_tienda_instrumento' => $model->codigo_tienda_instrumento], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigo_tienda_instrumento' => $model->codigo_tienda_instrumento], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_tienda_instrumento',
            'url_tienda_instrumentos:url',
        ],
    ]) ?>

</div>
