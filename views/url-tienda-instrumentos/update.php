<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\UrlTiendaInstrumentos $model */

$this->title = 'Update Url Tienda Instrumentos: ' . $model->codigo_tienda_instrumento;
$this->params['breadcrumbs'][] = ['label' => 'Url Tienda Instrumentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_tienda_instrumento, 'url' => ['view', 'codigo_tienda_instrumento' => $model->codigo_tienda_instrumento]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="url-tienda-instrumentos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
