<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\UrlTiendaInstrumentos $model */

$this->title = 'Crear Tienda Instrumentos';
$this->params['breadcrumbs'][] = ['label' => 'Url Tienda Instrumentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="url-tienda-instrumentos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
