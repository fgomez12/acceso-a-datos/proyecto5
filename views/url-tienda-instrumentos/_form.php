<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\UrlTiendaInstrumentos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="url-tienda-instrumentos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'url_tienda_instrumentos')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
