<?php

use app\models\UrlTiendaInstrumentos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Tiendas de Instrumentos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="url-tienda-instrumentos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Tienda de Instrumentos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'codigo_tienda_instrumento',
            'url_tienda_instrumentos:url',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, UrlTiendaInstrumentos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo_tienda_instrumento' => $model->codigo_tienda_instrumento]);
                 }
            ],
        ],
    ]); ?>


</div>
