<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Instrumentos $model */

$this->title = 'Update Instrumentos: ' . $model->codigo_instrumento;
$this->params['breadcrumbs'][] = ['label' => 'Instrumentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_instrumento, 'url' => ['view', 'codigo_instrumento' => $model->codigo_instrumento]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="instrumentos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
