<?php

use app\models\Instrumentos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Instrumentos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instrumentos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Instrumento', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'codigo_instrumento',
            'tipo',
            'marca',
            'modelo',
            'tamaño',
            'material',
            //'codigo_tienda',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Instrumentos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo_instrumento' => $model->codigo_instrumento]);
                 }
            ],
        ],
    ]); ?>


</div>
