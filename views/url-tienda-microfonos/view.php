<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\UrlTiendaMicrofonos $model */

$this->title = $model->codigo_tienda_microfonos;
$this->params['breadcrumbs'][] = ['label' => 'Url Tienda Microfonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="url-tienda-microfonos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigo_tienda_microfonos' => $model->codigo_tienda_microfonos], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigo_tienda_microfonos' => $model->codigo_tienda_microfonos], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_tienda_microfonos',
            'url_tienda_microfonos:url',
        ],
    ]) ?>

</div>
