<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\UrlTiendaMicrofonos $model */

$this->title = 'Update Url Tienda Microfonos: ' . $model->codigo_tienda_microfonos;
$this->params['breadcrumbs'][] = ['label' => 'Url Tienda Microfonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_tienda_microfonos, 'url' => ['view', 'codigo_tienda_microfonos' => $model->codigo_tienda_microfonos]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="url-tienda-microfonos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
