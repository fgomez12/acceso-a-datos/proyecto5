<?php

use app\models\UrlTiendaMicrofonos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Tiendas de Micrófonos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="url-tienda-microfonos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Tienda de Micrófonos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'codigo_tienda_microfonos',
            'url_tienda_microfonos:url',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, UrlTiendaMicrofonos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo_tienda_microfonos' => $model->codigo_tienda_microfonos]);
                 }
            ],
        ],
    ]); ?>


</div>
