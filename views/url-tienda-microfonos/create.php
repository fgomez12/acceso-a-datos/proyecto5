<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\UrlTiendaMicrofonos $model */

$this->title = 'Crear Tienda Microfonos';
$this->params['breadcrumbs'][] = ['label' => 'Url Tienda Microfonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="url-tienda-microfonos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
