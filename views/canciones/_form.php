<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Estudios;

/** @var yii\web\View $this */
/** @var app\models\Canciones $model */
/** @var yii\widgets\ActiveForm $form */

$estudios = ArrayHelper::map( Estudios::find()->all(), 'codigo_estudio', 'nombre' );

?>

<div class="canciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'interprete')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_album')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_usuario')->textInput() ?>

    <?php //$form->field($model, 'codigo_estudio')->textInput() ?>
    
    <?= $form->field($model, 'codigo_estudio')->dropDownList($estudios, ['prompt' => 'Seleccione Uno' ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
