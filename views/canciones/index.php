<?php

use app\models\Canciones;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use app\models\Estudios;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */


$this->title = 'Canciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="canciones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Canción', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'codigo_cancion',
            'interprete',
            'titulo',
            'nombre_album',
            //'codigo_usuario',
            //'codigo_estudio',
            [
                'attribute' => 'codigo_estudio',

                'label' => 'Estudio',

                'value' => function($model, $index, $dataColumn) {
                    return $model->codigo_estudio;

                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Canciones $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo_cancion' => $model->codigo_cancion]);
                 }
            ],
        ],
    ]); ?>


</div>