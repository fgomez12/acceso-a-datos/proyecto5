<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Sonorizan $model */

$this->title = 'Crear Sonorizan';
$this->params['breadcrumbs'][] = ['label' => 'Sonorizans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sonorizan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
