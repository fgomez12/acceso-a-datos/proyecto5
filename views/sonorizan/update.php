<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Sonorizan $model */

$this->title = 'Update Sonorizan: ' . $model->codigo_sonorizan;
$this->params['breadcrumbs'][] = ['label' => 'Sonorizans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_sonorizan, 'url' => ['view', 'codigo_sonorizan' => $model->codigo_sonorizan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sonorizan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
