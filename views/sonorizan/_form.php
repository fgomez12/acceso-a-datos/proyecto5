<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Sonorizan $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="sonorizan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_sonorizan')->textInput() ?>

    <?= $form->field($model, 'codigo_instrumento')->textInput() ?>

    <?= $form->field($model, 'codigo_microfono')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
