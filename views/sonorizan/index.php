<?php

use app\models\Sonorizan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Sonorizan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sonorizan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Sonorizan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_sonorizan',
            'codigo_instrumento',
            'codigo_microfono',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Sonorizan $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo_sonorizan' => $model->codigo_sonorizan]);
                 }
            ],
        ],
    ]); ?>


</div>
