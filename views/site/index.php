<?php

/** @var yii\web\View $this */
/** @var $canciones app\models\Canciones[]*/
use yii\helpers\Html;
$this->title = 'Grabaciones';
?>

<h1><?=Html::encode($this->title)?></h1>


<table class="table table-bordered"> <!-- creacion de tabla de informacion de cancion -->
    <thead>
        <tr>
            <th>
                Intérprete
            </th>
            <th>
                Título
            </th>
            <th>
                Álbum
            </th>
       </tr>
    </thead>
    <tbody>
        <?php foreach($canciones as $cancion):?>
        <tr>
            <td>
              <?=Html::encode($cancion->interprete)?>
            </td>
            <td>
              <?=Html::encode($cancion->titulo)?>
            </td><!-- comment -->
            <td>
              <?=Html::encode($cancion->album)?>
            </td><!-- comment -->
            
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>