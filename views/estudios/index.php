<?php

use app\models\Estudios;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Estudios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estudios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Estudio', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'codigo_estudio',
            'nombre',
            'web',
            'tecnico_sonido',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Estudios $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo_estudio' => $model->codigo_estudio]);
                 }
            ],
        ],
    ]); ?>


</div>
