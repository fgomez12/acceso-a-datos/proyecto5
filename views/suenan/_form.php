<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Suenan $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="suenan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_suenan')->textInput() ?>

    <?= $form->field($model, 'codigo_cancion')->textInput() ?>

    <?= $form->field($model, 'codigo_instrumento')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
