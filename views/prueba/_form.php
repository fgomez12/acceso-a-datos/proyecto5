<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Prueba $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="prueba-form">

    
<?php echo $form->dropDownList($model,
'id',
CHtml::listData(CnArticuloGrupo::model()->findAll(), 'id', 'sexo')
);?>
    <div class="form-prueba">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


