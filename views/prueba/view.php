<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;


/** @var yii\web\View $this */
/** @var app\models\Prueba $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pruebas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="prueba-view">

    <h1><?= Html::encode($this->title) ?></h1>

    
    echo $form->field($model, 'id')->widget(DepDrop::classname(), [
    'options'=>['id'=>'subcat-id'],
    'pluginOptions'=>[
        'depends'=>['cat-id'],
        'placeholder'=>'Selecciona un subcategoria...',
        'url'=>Url::to(['/site/subcat'])
    ]
]);

   

</div>
