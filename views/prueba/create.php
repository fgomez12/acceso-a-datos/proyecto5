<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Prueba $model */

$this->title = 'Create Prueba';
$this->params['breadcrumbs'][] = ['label' => 'Pruebas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prueba-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
