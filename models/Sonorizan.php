<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sonorizan".
 *
 * @property int $codigo_sonorizan
 * @property int|null $codigo_instrumento
 * @property int|null $codigo_microfono
 *
 * @property Instrumentos $codigoInstrumento
 * @property Microfonos $codigoMicrofono
 */
class Sonorizan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sonorizan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_sonorizan'], 'required'],
            [['codigo_sonorizan', 'codigo_instrumento', 'codigo_microfono'], 'integer'],
            [['codigo_microfono', 'codigo_instrumento'], 'unique', 'targetAttribute' => ['codigo_microfono', 'codigo_instrumento']],
            [['codigo_sonorizan'], 'unique'],
            [['codigo_instrumento'], 'exist', 'skipOnError' => true, 'targetClass' => Instrumentos::class, 'targetAttribute' => ['codigo_instrumento' => 'codigo_instrumento']],
            [['codigo_microfono'], 'exist', 'skipOnError' => true, 'targetClass' => Microfonos::class, 'targetAttribute' => ['codigo_microfono' => 'codigo_microfono']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_sonorizan' => 'Codigo Sonorizan',
            'codigo_instrumento' => 'Codigo Instrumento',
            'codigo_microfono' => 'Codigo Microfono',
        ];
    }

    /**
     * Gets query for [[CodigoInstrumento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoInstrumento()
    {
        return $this->hasOne(Instrumentos::class, ['codigo_instrumento' => 'codigo_instrumento']);
    }

    /**
     * Gets query for [[CodigoMicrofono]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMicrofono()
    {
        return $this->hasOne(Microfonos::class, ['codigo_microfono' => 'codigo_microfono']);
    }
}
