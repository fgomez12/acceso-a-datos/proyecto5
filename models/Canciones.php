<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "canciones".
 *
 * @property int $codigo_cancion
 * @property string|null $interprete
 * @property string|null $titulo
 * @property string|null $nombre_album
 * @property int|null $codigo_usuario
 * @property int|null $codigo_estudio
 *
 * @property Estudios $codigoEstudio
 * @property Instrumentos[] $codigoInstrumentos
 * @property Usuarios $codigoUsuario
 * @property Suenan[] $suenans
 */
class Canciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'canciones';
    }

    /**
     * {@inheritdoc}
     */
   public function rules()
    {
        return [
            [['codigo_usuario', 'codigo_estudio'], 'required'],
            [['codigo_usuario', 'codigo_estudio'], 'integer'],
            [['interprete'], 'required'],
            [['interprete'], 'string', 'max' => 50],
            [['titulo'], 'required'],
            [['titulo', 'nombre_album'], 'string', 'max' => 70],
            [['codigo_estudio'], 'exist', 'skipOnError' => true, 'targetClass' => Estudios::class, 'targetAttribute' => ['codigo_estudio' => 'codigo_estudio']],
            [['codigo_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::class, 'targetAttribute' => ['codigo_usuario' => 'codigo_usuario']],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_cancion' => 'Codigo Cancion',
            'interprete' => 'Interprete',
            'titulo' => 'Titulo',
            'nombre_album' => 'Nombre Album',
            'codigo_usuario' => 'Codigo Usuario',
            'codigo_estudio' => 'Codigo Estudio',
        ];
    }

    /**
     * Gets query for [[CodigoEstudio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEstudio()
    {
        return $this->hasOne(Estudios::class, ['codigo_estudio' => 'codigo_estudio']);
    }
    
    /**
     * Gets query for [[CodigoInstrumentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoInstrumentos()
    {
        return $this->hasMany(Instrumentos::class, ['codigo_instrumento' => 'codigo_instrumento'])->viaTable('suenan', ['codigo_cancion' => 'codigo_cancion']);
    }

    /**
     * Gets query for [[CodigoUsuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoUsuario()
    {
        return $this->hasOne(Usuarios::class, ['codigo_usuario' => 'codigo_usuario']);
    }

    /**
     * Gets query for [[Suenans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSuenans()
    {
        return $this->hasMany(Suenan::class, ['codigo_cancion' => 'codigo_cancion']);
    }
}
