<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estudios".
 *
 * @property int $codigo_estudio
 * @property string|null $nombre
 * @property string|null $web
 * @property string|null $tecnico_sonido
 *
 * @property Canciones[] $canciones
 */
class Estudios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estudios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'web', 'tecnico_sonido'], 'required'],
            [['nombre', 'web'], 'string', 'max' => 50],
            [['web'], 'url', 'defaultScheme' => 'http'],
            [['tecnico_sonido'], 'string', 'max' => 70],
            [['codigo_estudio'], 'exist', 'skipOnError' => true, 'targetClass' => Canciones::class, 'targetAttribute' => ['codigo_estudio' => 'codigo_cancion']],
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_estudio' => 'Codigo Estudio',
            'nombre' => 'Nombre',
            'web' => 'Web',
            'tecnico_sonido' => 'Tecnico Sonido',
        ];
    }

    /**
     * Gets query for [[Canciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCanciones()
    {
        return $this->hasMany(Canciones::class, ['codigo_estudio' => 'codigo_estudio']);
    }
}
