<?php
class TipoInstrumento {
    const BOMBO = "Bombo";
    const CAJA = "Caja";
    const HIHAT = "HiHat";
    const RIDE = "Ride";
}
?>