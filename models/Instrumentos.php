<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "instrumentos".
 *
 * @property int $codigo_instrumento
 * @property string|null $tipo
 * @property string|null $marca
 * @property string|null $modelo
 * @property string|null $tamaño
 * @property string|null $material
 * @property int|null $codigo_tienda
 *
 * @property Canciones[] $codigoCancions
 * @property Microfonos[] $codigoMicrofonos
 * @property UrlTiendaInstrumentos $codigoTienda
 * @property Sonorizan[] $sonorizans
 * @property Suenan[] $suenans
 */
class Instrumentos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'instrumentos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_tienda'], 'integer'],
            [['tipo'], 'string', 'max' => 20],
            [['marca', 'modelo', 'tamaño'], 'string', 'max' => 50],
            [['material'], 'string', 'max' => 30],
            [['codigo_tienda'], 'exist', 'skipOnError' => true, 'targetClass' => UrlTiendaInstrumentos::class, 'targetAttribute' => ['codigo_tienda' => 'codigo_tienda_instrumento']],
            [['marca', 'modelo', 'tamaño'], 'required'],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_instrumento' => 'Codigo Instrumento',
            'tipo' => 'Tipo',
            'marca' => 'Marca',
            'modelo' => 'Modelo',
            'tamaño' => 'Tamaño',
            'material' => 'Material',
            'codigo_tienda' => 'Codigo Tienda',
        ];
    }

    /**
     * Gets query for [[CodigoCancions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCancions()
    {
        return $this->hasMany(Canciones::class, ['codigo_cancion' => 'codigo_cancion'])->viaTable('suenan', ['codigo_instrumento' => 'codigo_instrumento']);
    }

    /**
     * Gets query for [[CodigoMicrofonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMicrofonos()
    {
        return $this->hasMany(Microfonos::class, ['codigo_microfono' => 'codigo_microfono'])->viaTable('sonorizan', ['codigo_instrumento' => 'codigo_instrumento']);
    }

    /**
     * Gets query for [[CodigoTienda]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoTienda()
    {
        return $this->hasOne(UrlTiendaInstrumentos::class, ['codigo_tienda_instrumento' => 'codigo_tienda']);
    }

    /**
     * Gets query for [[Sonorizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSonorizans()
    {
        return $this->hasMany(Sonorizan::class, ['codigo_instrumento' => 'codigo_instrumento']);
    }

    /**
     * Gets query for [[Suenans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSuenans()
    {
        return $this->hasMany(Suenan::class, ['codigo_instrumento' => 'codigo_instrumento']);
    }
}
