<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "suenan".
 *
 * @property int $codigo_suenan
 * @property int|null $codigo_cancion
 * @property int|null $codigo_instrumento
 *
 * @property Canciones $codigoCancion
 * @property Instrumentos $codigoInstrumento
 */
class Suenan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'suenan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_suenan'], 'required'],
            [['codigo_suenan', 'codigo_cancion', 'codigo_instrumento'], 'integer'],
            [['codigo_cancion', 'codigo_instrumento'], 'unique', 'targetAttribute' => ['codigo_cancion', 'codigo_instrumento']],
            [['codigo_suenan'], 'unique'],
            [['codigo_cancion'], 'exist', 'skipOnError' => true, 'targetClass' => Canciones::class, 'targetAttribute' => ['codigo_cancion' => 'codigo_cancion']],
            [['codigo_instrumento'], 'exist', 'skipOnError' => true, 'targetClass' => Instrumentos::class, 'targetAttribute' => ['codigo_instrumento' => 'codigo_instrumento']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_suenan' => 'Codigo Suenan',
            'codigo_cancion' => 'Codigo Cancion',
            'codigo_instrumento' => 'Codigo Instrumento',
        ];
    }

    /**
     * Gets query for [[CodigoCancion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCancion()
    {
        return $this->hasOne(Canciones::class, ['codigo_cancion' => 'codigo_cancion']);
    }

    /**
     * Gets query for [[CodigoInstrumento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoInstrumento()
    {
        return $this->hasOne(Instrumentos::class, ['codigo_instrumento' => 'codigo_instrumento']);
    }
}
