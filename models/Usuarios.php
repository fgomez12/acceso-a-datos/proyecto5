<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $codigo_usuario
 * @property string|null $nombre
 * @property string|null $contraseña
 *
 * @property Canciones[] $canciones
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'contraseña'], 'required'],
            [['nombre'], 'unique'],
            [['nombre'], 'string', 'max' => 30],
            [['contraseña'], 'string', 'min' => 8, 'max' => 30],
            [['contraseña'], 'match', 'pattern' => '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/', 'message' => 'La contraseña debe contener al menos una minúscula, una mayúscula y un número.'],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_usuario' => 'Codigo Usuario',
            'nombre' => 'Nombre',
            'contraseña' => 'Contraseña',
        ];
    }

    /**
     * Gets query for [[Canciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCanciones()
    {
        return $this->hasMany(Canciones::class, ['codigo_usuario' => 'codigo_usuario']);
    }
}
