<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "url_tienda_instrumentos".
 *
 * @property int $codigo_tienda_instrumento
 * @property string|null $url_tienda_instrumentos
 *
 * @property Instrumentos[] $instrumentos
 */
class UrlTiendaInstrumentos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'url_tienda_instrumentos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url_tienda_instrumentos'], 'required'],
            [['url_tienda_instrumentos'], 'string', 'max' => 200],
            [['url_tienda_instrumentos'], 'url'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_tienda_instrumento' => 'Codigo Tienda Instrumento',
            'url_tienda_instrumentos' => 'Url Tienda Instrumentos',
        ];
    }

    /**
     * Gets query for [[Instrumentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInstrumentos()
    {
        return $this->hasMany(Instrumentos::class, ['codigo_tienda' => 'codigo_tienda_instrumento']);
    }
}
