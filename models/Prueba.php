<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prueba".
 *
 * @property int $id
 * @property string|null $sexo
 */
class Prueba extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prueba';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sexo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sexo' => 'Sexo',
        ];
    }
    
    public function getPrueba($id=0)
{
$criteria=new CDbCriteria();
$criteria->select='id,sexo'; //solo quiero estos dos campos
$criteria->order='ordenPresentacion'; //para que aparezcan clasificados
$criteria->addCondition('id=1'); //solo los que sean del idioma 1
if ($id>0)
$criteria->addCondition('id='.$id); //Si me indican id //busco  uno
$aPrueba=$this->findAll($criteria); //lanzo el query
$prueba=array();
foreach ($Prueba as $p)
$prueba($p->id)= $p->Descripcion_Prueba; //monto array
return $prueba;
}
}
