<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "microfonos".
 *
 * @property int $codigo_microfono
 * @property string|null $tipo
 * @property string|null $marca
 * @property string|null $modelo
 * @property int|null $codigo_tienda_microfonos
 *
 * @property Instrumentos[] $codigoInstrumentos
 * @property UrlTiendaMicrofonos $codigoTiendaMicrofonos
 * @property Sonorizan[] $sonorizans
 */
class Microfonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'microfonos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_tienda_microfonos'], 'required'],
            [['codigo_tienda_microfonos'], 'integer'],
            [['tipo', 'marca', 'modelo'], 'required'],
            [['tipo'], 'string', 'max' => 20],
            [['marca', 'modelo'], 'string', 'max' => 50],
            [['codigo_tienda_microfonos'], 'exist', 'skipOnError' => true, 'targetClass' => UrlTiendaMicrofonos::class, 'targetAttribute' => ['codigo_tienda_microfonos' => 'codigo_tienda_microfonos']],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_microfono' => 'Codigo Microfono',
            'tipo' => 'Tipo',
            'marca' => 'Marca',
            'modelo' => 'Modelo',
            'codigo_tienda_microfonos' => 'Codigo Tienda Microfonos',
        ];
    }

    /**
     * Gets query for [[CodigoInstrumentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoInstrumentos()
    {
        return $this->hasMany(Instrumentos::class, ['codigo_instrumento' => 'codigo_instrumento'])->viaTable('sonorizan', ['codigo_microfono' => 'codigo_microfono']);
    }

    /**
     * Gets query for [[CodigoTiendaMicrofonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoTiendaMicrofonos()
    {
        return $this->hasOne(UrlTiendaMicrofonos::class, ['codigo_tienda_microfonos' => 'codigo_tienda_microfonos']);
    }

    /**
     * Gets query for [[Sonorizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSonorizans()
    {
        return $this->hasMany(Sonorizan::class, ['codigo_microfono' => 'codigo_microfono']);
    }
}
