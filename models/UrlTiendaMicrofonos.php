<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "url_tienda_microfonos".
 *
 * @property int $codigo_tienda_microfonos
 * @property string|null $url_tienda_microfonos
 *
 * @property Microfonos[] $microfonos
 */
class UrlTiendaMicrofonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'url_tienda_microfonos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url_tienda_microfonos'], 'required'],
            [['url_tienda_microfonos'], 'string', 'max' => 200],
            [['url_tienda_microfonos'], 'url'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_tienda_microfonos' => 'Codigo Tienda Microfonos',
            'url_tienda_microfonos' => 'Url Tienda Microfonos',
        ];
    }

    /**
     * Gets query for [[Microfonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMicrofonos()
    {
        return $this->hasMany(Microfonos::class, ['codigo_tienda_microfonos' => 'codigo_tienda_microfonos']);
    }
}
