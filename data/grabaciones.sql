﻿/* creación de la base de datos */

DROP DATABASE IF EXISTS grabaciones;
CREATE DATABASE grabaciones;

USE grabaciones;

/* creacion de las tablas */

  -- usuarios

  CREATE OR REPLACE TABLE usuarios(
    codigo_usuario int(9) AUTO_INCREMENT,
    nombre varchar(30),
    contraseña varchar(30),
    PRIMARY KEY (codigo_usuario)
  );

-- canciones

  CREATE OR REPLACE TABLE canciones(
    codigo_cancion int(10) AUTO_INCREMENT,
    interprete varchar(50),
    titulo varchar(70),
    nombre_album varchar(70),
     codigo_usuario int(9),
     codigo_estudio int(9),
    PRIMARY KEY (codigo_cancion)
  );

-- instrumentos

  CREATE OR REPLACE TABLE instrumentos(
    codigo_instrumento int(9) AUTO_INCREMENT,
    tipo varchar(20),
    marca varchar (50),
    modelo varchar(50),
    tamaño varchar(50),
    material varchar(30),
    codigo_tienda int(9),
    PRIMARY KEY(codigo_instrumento)
  );

-- url_tienda_instrumentos

  CREATE OR REPLACE TABLE url_tienda_instrumentos(
    codigo_tienda_instrumento int(9) AUTO_INCREMENT,
    url_tienda_instrumentos varchar(200),
    PRIMARY KEY(codigo_tienda_instrumento)
  );

-- microfonos

   CREATE OR REPLACE TABLE microfonos(
    codigo_microfono int(9) AUTO_INCREMENT,
    tipo varchar(20),
    marca varchar (50),
    modelo varchar(50),
    codigo_tienda_microfonos int(9),
    PRIMARY KEY(codigo_microfono)
  );

-- url_tienda_microfonos

  CREATE OR REPLACE TABLE url_tienda_microfonos(
    codigo_tienda_microfonos int(9) AUTO_INCREMENT,
    url_tienda_microfonos varchar(200),
    PRIMARY KEY(codigo_tienda_microfonos)
  );

-- estudios

   CREATE OR REPLACE TABLE estudios(
    codigo_estudio int(9) AUTO_INCREMENT,
    nombre varchar(50),
    web varchar (50),
    tecnico_sonido varchar(70),
    PRIMARY KEY(codigo_estudio)
  );

-- suenan

 CREATE OR REPLACE TABLE suenan(
    codigo_suenan int (9) , 
    codigo_cancion int(10), 
    codigo_instrumento int(9),
    PRIMARY KEY(codigo_suenan)
  );

-- sonorizan

 CREATE OR REPLACE TABLE sonorizan(
    codigo_sonorizan int (9),
    codigo_instrumento int(9),
    codigo_microfono int(9), 
    PRIMARY KEY(codigo_sonorizan)
  );

/** creacion de las restricciones **/

-- canciones

ALTER TABLE canciones
  ADD CONSTRAINT fk_codigo_usuario
  FOREIGN KEY (codigo_usuario)
  REFERENCES usuarios(codigo_usuario);

ALTER TABLE canciones
  ADD CONSTRAINT fk_codigo_estudio
  FOREIGN KEY (codigo_estudio)
  REFERENCES estudios(codigo_estudio);

-- url_tienda_instrumentos

ALTER TABLE instrumentos
  ADD CONSTRAINT fk_codigo_tienda
  FOREIGN KEY (codigo_tienda)
  REFERENCES url_tienda_instrumentos(codigo_tienda_instrumento);

-- microfonos

ALTER TABLE microfonos
  ADD CONSTRAINT fk_codigo_tienda_m
  FOREIGN KEY (codigo_tienda_microfonos)
  REFERENCES url_tienda_microfonos(codigo_tienda_microfonos);

-- suenan


ALTER TABLE suenan
	ADD CONSTRAINT fk_suenan_canciones
	FOREIGN KEY (codigo_cancion)
	REFERENCES canciones(codigo_cancion),

  ADD CONSTRAINT fk_suenan_instrumentos
	FOREIGN KEY (codigo_instrumento)
	REFERENCES instrumentos(codigo_instrumento),

	ADD CONSTRAINT uk_suenan
	UNIQUE KEY (codigo_cancion,codigo_instrumento)
;

-- sonorizan


ALTER TABLE sonorizan
	ADD CONSTRAINT fk_sonorizan_microfonos
	FOREIGN KEY (codigo_microfono)
	REFERENCES microfonos(codigo_microfono),

  ADD CONSTRAINT fk_sonorizan_instrumentos
	FOREIGN KEY (codigo_instrumento)
	REFERENCES instrumentos(codigo_instrumento),

	ADD CONSTRAINT uk_sonorizan
	UNIQUE KEY (codigo_microfono,codigo_instrumento)
;

